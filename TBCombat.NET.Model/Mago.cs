﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBCombat.NET.Utils;

namespace TBCombat.NET.Model
{
    /// <summary>
    /// Extends Jugador and represents a wizard (Jugador = player).
    /// 
    /// Copyright 2016 David Olmos Garrido
    /// </summary>
    class Mago : Jugador
    {
        private const int MAX_HP = 1200;
        private const int MAX_MP = 3000;

        public override Menu SkillMenu
        {
            get
            {
                return (target) => "Tus ataques:" + Environment.NewLine
                + "1. Disparo Mágico. Ataca con " + calculateRealDamage(1, target) + " puntos de daño. Recupera 30 de MP." + Environment.NewLine
                + "2. Cañonazo Rúnico. Ataca con " + calculateRealDamage(2, target) + " puntos de daño. Consume 100 de MP." + Environment.NewLine
                + "3. Sellado. Inmoviliza al adversario durante 2 turnos. Causa " + calculateRealDamage(1.0/3, target) + " de daño. Consume 400 de MP." + Environment.NewLine
                + "4. Bendición Arcana. Aumenta el ataque en 50 y la defensa en 30 durante 2 turnos. Consume 300 de MP." + Environment.NewLine
                + "5. Curación Sagrada. Cura 200 de HP al principio de cada turno durante 4 turnos. Consume 500 de MP.";
            }
        }

        public Mago(string n)
            :base(n, Clases.Mago, MAX_HP, MAX_MP, 100, 50, 10)
        {
            Ataques[0] = (target) =>
            {
                if (MP + 30 <= MAX_MP)
                {
                    MP += 30;
                }
                else
                {
                    MP = MAX_MP;
                }
                target.HP -= Atq;
            };

            Ataques[1] = (target) =>
            {
                if (MP >= 100)
                {
                    MP -= 100;
                    target.HP -= Atq * 2;
                }
                else
                {
                    Console.WriteLine(Nombre + Reference.NOT_MP);
                }
            };

            Ataques[2] = (target) =>
            {
                if (MP >= 400)
                {
                    MP -= 400;
                    Estado.StateModifier applicator = (player) => { player.WillAttack = false; };
                    Estado.StateModifier removal = (player) => { player.WillAttack = true; };
                    target.addState(new Estado(applicator, 1, removal, 2, Estado.Estados.Paralizado), false);
                    target.HP -= Atq / 3;
                }
                else
                {
                    Console.WriteLine(Nombre + Reference.NOT_MP);
                }
            };

            Ataques[3] = (target) =>
            {
                if (MP >= 300)
                {
                    MP -= 300;
                    Estado.StateModifier applicator = (player) =>
                    {
                        player.Atq += 50;
                        player.Def += 30;
                    };
                    Estado.StateModifier removal = (player) =>
                    {
                        player.Atq -= 50;
                        player.Def -= 30;
                    };
                    addState(new Estado(applicator, 1, removal, 2, Estado.Estados.StatsUp), false);
                }
                else
                {
                    Console.WriteLine(Nombre + Reference.NOT_MP);
                }
            };

            Ataques[4] = (target) =>
            {
                if (MP >= 500)
                {
                    MP -= 500;
                    Estado.StateModifier applicator = (player) => { player.hp += 200; };
                    Estado.StateModifier removal = (player) => { };
                    addState(new Estado(applicator, 3, removal, 3, Estado.Estados.Curandose), false);
                }
                else
                {
                    Console.WriteLine(Nombre + Reference.NOT_MP);
                }
            };
        }

        public override int seleccionAtaqueIA(Jugador player)
        {
            if (HP <= 300 && MP >= 500 && !isTaggedAs(Estado.Estados.Curandose))
            {
                return 4;
            }
            else if (isTaggedAs(Estado.Estados.Curandose) && MP >= 400 && !player.isTaggedAs(Estado.Estados.Paralizado))
            {
                return 2;
            }
            else if (isTaggedAs(Estado.Estados.Curandose) && player.isTaggedAs(Estado.Estados.Paralizado) && MP >= 300)
            {
                return 3;
            }
            else if (MP >= 100)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
