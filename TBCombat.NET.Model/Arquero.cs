﻿using System;

namespace TBCombat.NET.Model
{
    /// <summary>
    /// Extends Jugador and represents an archer (Jugador = player).
    /// 
    /// Copyright 2016 David Olmos Garrido
    /// </summary>
    class Arquero : Jugador
    {
        public const int HP_MAX = 2000, MP_MAX = 1700;

        public override Menu SkillMenu
        {
            get
            {
                return (target) => "Tus ataques:" + Environment.NewLine
                + "1. Flechazo. Ataca con " + calculateRealDamage(1, target) + " puntos de daño. Recupera 20 de MP." + Environment.NewLine
                + "2. Disparo Certero. Ataca con " + calculateRealDamage(2, target) + " puntos de daño. Consume 300 de MP." + Environment.NewLine
                + "3. Cambiazo. Roba 300 de MP al rival y causa " + calculateRealDamage(1 / 3.0, target) + " de daño. Recupera 300 de MP." + Environment.NewLine
                + "4. Corte Profundo. Causa sangrado al enemigo que pierde 75 de HP durante 4 turnos y realiza un daño de " + calculateRealDamage(1.0 / 4, target) + ". Consume 350 de MP." + Environment.NewLine
                + "5. Precisión Aumentada. Aumenta en 50 el ataque durante 3 turnos. Consume 300 de MP.";
            }
        }

        public Arquero(string n)
            :base(n, Jugador.Clases.Arquero, HP_MAX, MP_MAX, 200, 30, 30)
        {
            Ataques[0] = (target) =>
            {
                if (MP + 20 <= 1700)
                {
                    MP += 20;
                }
                else {
                    MP = 1700;
                }
                target.HP -= Atq;
            };

            Ataques[1] = (target) =>
            {
                if (MP >= 300)
                {
                    MP -= 300;
                    target.HP -= Atq * 2;
                }
                else
                {
                    Console.WriteLine(Nombre + Utils.Reference.NOT_MP);
                }
            };

            Ataques[2] = target =>
            {
                if (target.MP >= 300)
                {
                    target.MP -= 300;
                    if (MP <= MP_MAX - 300)
                    {
                        MP += 300;
                    }
                    else
                    {
                        MP = MP_MAX;
                    }
                }
                else
                {
                    MP += target.MP;
                    target.MP = 0;
                }
                target.HP -= Atq/3;
            };

            Ataques[3] = (target) =>
            {
                if(MP >= 350)
                {
                    MP -= 350;
                    Estado.StateModifier applicator = (player) => { player.HP -= 75; };
                    Estado.StateModifier removal = (player) => { };
                    target.addState(new Estado(applicator, 4, removal, 4, Estado.Estados.Sangrado), false);
                    target.HP -= Atq / 4;
                }
                else
                {
                    Console.WriteLine(Nombre + Utils.Reference.NOT_MP);
                }
            };

            Ataques[4] = (target) =>
            {
                if(MP >= 300)
                {
                    Estado.StateModifier applicator = (player) => { player.Atq += 50; };
                    Estado.StateModifier removal = (player) => { player.Atq -= 50; };
                    addState(new Estado(applicator, 1, removal, 3, Estado.Estados.StatsUp), false);
                    MP -= 300;
                }
                else
                {
                    Console.WriteLine(Nombre + Utils.Reference.NOT_MP);
                }
            };
        }

        public override int seleccionAtaqueIA(Jugador player)
        {
            return 1;
        }

    }


}
