﻿using System.IO;
using System.Windows.Controls;

namespace TBCombat.NET.Utils
{
    /// <summary>
    /// Interface to implement at forms which replace the default console.
    /// 
    /// Copyright 2016 David Olmos Garrido
    /// </summary>
    public interface IConsoleForm
    {
        TextBox NewConsole { get; }
        TextWriter OldConsole { get; set; }
    }
}
