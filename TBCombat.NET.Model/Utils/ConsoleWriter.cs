﻿using System;
using System.Text;
using System.IO;

namespace TBCombat.NET.Utils
{
    /// <summary>
    /// A custom TextWriter.
    /// 
    /// Copyright 2016 David Olmos Garrido
    /// </summary>
    public class ConsoleWriter: TextWriter
    {
        public override Encoding Encoding
        {
            get
            {
                return Encoding.ASCII;
            }
        }


        private IConsoleForm form;
        public ConsoleWriter(IConsoleForm form)
        {
            this.form = form;
            form.OldConsole = Console.Out;
        }

        public override void Write(char value)
        {
            form.NewConsole.Text += value;
        }

        public override void Write(string value)
        {
            form.NewConsole.Text += value;
        }
    }
}
