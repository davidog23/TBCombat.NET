﻿using System;
using System.Collections.Generic;

namespace TBCombat.NET.Model
{
    /// <summary>
    /// Represents a state. It contains a Effect (a delegate which modifies an Jugador) to apply, 
    /// a Effect to remove (the opposite you have applied), 
    /// the number of turns the effect is executed and the number of turns until the effect is removed.
    /// 
    /// Copyright 2016 David Olmos Garrido
    /// </summary>
    public class Estado: IEquatable<Estado>
    {
        public int Contador { get; set; }
        public List<Estados> Tag { get; set; }
        private StateModifier applicator;
        private int applicationCount;
        public StateModifier StateApplicator
        {
            get
            {
                if(applicationCount > 0)
                {
                    applicationCount--;
                    return applicator;
                }
                return (jugador) => { };
            }
        }
        public StateModifier StateRemoval { get; private set; }

        /// <summary>
        /// Constructor of Estado.
        /// </summary>
        /// <param name="applicator">A delegate which will be executed which modifies a Jugador.</param>
        /// <param name="timesToApplyApplicator">Time to apply the applicator.</param>
        /// <param name="removal">A delegate to clean up the Jugador object</param>
        /// <param name="turnsUntilRemoval">Turns until apply the removal</param>
        public Estado(StateModifier applicator, int timesToApplyApplicator, StateModifier removal, int turnsUntilRemoval)
        {
            this.applicator = applicator;
            StateRemoval = removal;
            applicationCount = timesToApplyApplicator;
            Contador = turnsUntilRemoval;
            Tag = new List<Estados>();
        }

        /// <summary>
        /// Constructor of Estado.
        /// </summary>
        /// <param name="applicator">A delegate which will be executed which modifies a Jugador.</param>
        /// <param name="timesToApplyApplicator">Time to apply the applicator.</param>
        /// <param name="removal">A delegate to clean up the Jugador object</param>
        /// <param name="turnsUntilRemoval">Turns until apply the removal</param>
        /// <param name="tags">An optional parameter with tags for the state.</param>
        public Estado(StateModifier applicator, int timesToApplyApplicator, StateModifier removal, int turnsUntilRemoval, params Estados[] tags)
            : this(applicator, timesToApplyApplicator, removal, turnsUntilRemoval)
        {
            Tag.AddRange(tags);
        }

        public delegate void StateModifier(Jugador jugador);

        public void putState(List<Estado> states)
        {
            if (states.Contains(this))
            {
                Estado appliedState = states.Find((applied) => applied.Equals(this));
                if (appliedState.Contador < Contador)
                {
                    applicationCount = 0;
                    states.Remove(appliedState);
                }
            }
            states.Add(this);
        }

        public bool Equals(Estado other)
        {
            return applicator == other.applicator && StateRemoval == other.StateRemoval;
        }

        /// <summary>
        /// Enum with the categories of the states (Paralizado = Stun, Curandose = Healing, Sangrado = Bleeding ...)
        /// </summary>
        public enum Estados
        {
            Paralizado, Curandose, Sangrado, StatsUp, StatsDown
        }

        public static string tagToString(Estados stateTag)
        {
            switch (stateTag)
            {
                case Estados.StatsDown:
                    return "Estadisticas bajadas";
                case Estados.StatsUp:
                    return "Estadisticas aumentadas";
                default:
                    return stateTag.ToString();
            }
        }
    }
    
}
