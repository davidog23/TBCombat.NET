﻿using System;
using TBCombat.NET.Utils;

namespace TBCombat.NET.Model
{
    /// <summary>
    /// Extends Jugador and represents a knight (Jugador = player).
    /// 
    /// Copyright 2016 David Olmos Garrido
    /// </summary>
    class Espadachin : Jugador
    {
        private const int MAX_HP = 3000;
        private const int MAX_MP = 900;

        public override Menu SkillMenu
        {
            get
            {
                return (target) => "Tus ataques:" + Environment.NewLine
                + "1. Ataque Básico. Ataca con " + calculateRealDamage(1, target) + " puntos de daño. Recupera 10 de MP." + Environment.NewLine
                + "2. Triple Golpe. Ataca con " + calculateRealDamage(3, target) + " puntos de daño. Consume 75 de MP." + Environment.NewLine
                + "3. Fisura de Hueso. Inmoviliza al adversario durante 2 turnos. Causa " + calculateRealDamage(1/2.0, target) + " de daño. Consume 150 de MP." + Environment.NewLine
                + "4. Moralizacion. Aumenta el ataque en 20 y la velocidad en 15 durante 3 turnos. Consume 150 de MP." + Environment.NewLine
                + "5. Escudo Férreo. Aumenta en 100 la defensa durante 4 turnos. Consume 300 de MP.";
            }
        }

        public Espadachin(string n)
            :base(n, Clases.Espadachin, MAX_HP, MAX_MP, 70, 100, 20)
        {
            Ataques[0] = (target) =>
            {
                if (MP + 10 <= MAX_MP)
                {
                    MP += 10;
                }
                else
                {
                    MP = MAX_MP;
                }
                target.HP -= Atq;
            };

            Ataques[1] = (target) =>
            {
                if (MP >= 75)
                {
                    MP -= 75;
                    target.HP -= Atq * 3;
                }
                else
                {
                    Console.WriteLine(Nombre + Reference.NOT_MP);
                }
            };

            Ataques[2] = (target) =>
            {
                if (MP >= 150)
                {
                    MP -= 150;
                    Estado.StateModifier applicator = (player) => { player.WillAttack = false; };
                    Estado.StateModifier removal = (player) => { player.WillAttack = true; };
                    target.addState(new Estado(applicator, 1, removal, 2, Estado.Estados.Paralizado), false);
                    target.HP -= Atq / 2;
                }
                else
                {
                    Console.WriteLine(Nombre + Reference.NOT_MP);
                }
            };

            Ataques[3] = (target) =>
            {
                if (MP >= 150)
                {
                    Estado.StateModifier applicator = (player) =>
                    {
                        player.Atq += 20;
                        player.Vel += 15;
                    };
                    Estado.StateModifier removal = (player) =>
                    {
                        player.Atq -= 20;
                        player.Vel -= 15;
                    };
                    addState(new Estado(applicator, 1, removal, 3, Estado.Estados.StatsUp), false);
                    MP -= 150;
                }
                else {
                    Console.WriteLine(Nombre + Reference.NOT_MP);
                }
            };

            Ataques[4] = (target) =>
            {
                if (MP >= 300)
                {
                    Estado.StateModifier applicator = (player) => { player.Def += 100; };
                    Estado.StateModifier removal = (player) => { player.Def -= 100; };
                    addState(new Estado(applicator, 1, removal, 4, Estado.Estados.StatsUp), false);
                    MP -= 300;
                }
                else
                {
                    Console.WriteLine(Nombre + Reference.NOT_MP);
                }
            };
        }

        public override int seleccionAtaqueIA(Jugador player)
        {
            return 1;
        }
    }
}
