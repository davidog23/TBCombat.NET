﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace TBCombat.NET.Model
{
    /// <summary>
    /// A class which represents a player. It's extended by Arquero, Espadachin and Mago.
    /// 
    /// Copyright 2016 David Olmos Garrido
    /// </summary>
    public abstract class Jugador : INotifyPropertyChanged
    {
        public static readonly double DEFAULT_DEF_REDUCTION = 0.45;
        public Guid MatchUUID { get; set; }
        public string Nombre { get; private set; }
        public Clases Clase { get; private set; }

        internal int hp;
        public int HP
        {
            get { return hp; }
            set
            {
                int realDamage = (hp - value) - (int)(Def * DEFAULT_DEF_REDUCTION);
                if(realDamage > 0)
                {
                    hp -= realDamage;
                    NotifyPropertyChanged("HP");
                }
            }
        }

        private int mp;
        public int MP
        {
            get { return mp; }
            set
            {
                if(mp != value)
                {
                    mp = value;
                    NotifyPropertyChanged("MP");
                }
                
            }
        }

        private int atq;
        public int Atq
        {
            get
            {
                return atq;
            }

            set
            {
                if (atq != value)
                {
                    atq = value;
                    NotifyPropertyChanged("AtqString"); 
                }
            }
        }
        public string AtqString { get { return "Attack: " + atq; } }

        private int def;
        public int Def
        {
            get
            {
                return def;
            }

            set
            {
                if (def != value)
                {
                    def = value;
                    NotifyPropertyChanged("DefString"); 
                }
            }
        }
        public string DefString { get { return "Defense: " + def; } }

        private int vel;
        public int Vel
        {
            get
            {
                return vel;
            }

            set
            {
                if (vel != value)
                {
                    vel = value;
                    NotifyPropertyChanged("VelString"); 
                }
            }
        }
        public string VelString { get { return "Speed: " + vel; } }

        public abstract Menu SkillMenu { get; }

        /// <summary>
        /// A willAttack field should be modified by StateModifier objects only.
        /// Furthermore, if a StateApplicator modifies it to false, a StateRemoval should set it to true again.
        /// </summary>
        public bool WillAttack { get; set; } = true;

        public Ataque[] Ataques { get; private set; }

        public List<Estado> Estados { get; set; }

        public bool Online { get; set; }

        protected Jugador(string n, Clases clase, int hp, int mp, int atq, int def, int vel)
        {
            Nombre = n;
            Clase = clase;
            this.hp = hp;
            MP = mp;
            Atq = atq;
            Def = def;
            Vel = vel;
            Online = false;
            Ataques = new Ataque[5];
            Estados = new List<Estado>();
        }

        public bool addState(Estado state, bool stackable)
        {
            if(stackable)
            {
                Estados.Add(state);
            }
            else
            {
                state.putState(Estados);
            }
            return true;
        }

        public void updateState()
        {
            foreach (Estado e in Estados)
            {
                if (e.Contador > 0) { e.StateApplicator(this); }
                else { e.StateRemoval(this); }
                e.Contador--;
            }
            Estados.RemoveAll((estado) => estado.Contador < 0);
            NotifyStatesUpdated();
        }

        public bool isTaggedAs(Estado.Estados tag)
        {
            return Estados.Exists(state => state.Tag.Count > 0 && state.Tag.Exists(tagInState => tagInState == tag));
        }

        public List<Estado.Estados> getUniqueTags()
        {
            List<Estado.Estados> res = new List<Estado.Estados>();
            Estados.ForEach((state) => res.AddRange(state.Tag.Where(tag => !res.Contains(tag))));
            return res;
        }

        public int calculateRealDamage(double atqMultiplier, Jugador other)
        {
            int damage = (int) (atq * atqMultiplier) - (int) (other.def * DEFAULT_DEF_REDUCTION);
            if (damage > 0) return damage;
            return 0;
        }

        public static Jugador init(KeyValuePair<string, Clases> inicializador)
        {
            switch (inicializador.Value)
            {
                case Clases.Arquero:
                    return new Arquero(inicializador.Key);
                case Clases.Espadachin:
                    return new Espadachin(inicializador.Key);
                case Clases.Mago:
                    return new Mago(inicializador.Key);
                default:
                    return null;
            }
        }

        public static Jugador init()
        {
            Random rand = new Random(DateTime.Now.Millisecond);
            switch(rand.Next(0, 3))
            {
                case 0:
                    return new Arquero("IA");
                case 1:
                    return new Espadachin("IA");
                case 2:
                    return new Mago("IA");
                default:
                    return null;
            }

        }

        public abstract int seleccionAtaqueIA(Jugador player);

        public delegate void Ataque(Jugador target);
        public delegate string Menu(Jugador target);
        public delegate void UpdateStatesEventHandler(Jugador sender, List<Estado.Estados> newStateList);

        public event PropertyChangedEventHandler PropertyChanged;
        public event UpdateStatesEventHandler StatesUpdated;

        protected virtual void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual void NotifyStatesUpdated()
        {
            var handler = StatesUpdated;
            if (handler != null) handler(this, getUniqueTags());
        }

        /// <summary>
        /// An enum with the classes which implement this abstract class.
        /// </summary>
        public enum Clases
        {
            Arquero,
            Espadachin,
            Mago
        }

    }

    
}
