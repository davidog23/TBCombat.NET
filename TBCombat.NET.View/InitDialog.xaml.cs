﻿using System;
using System.Collections.Generic;
using System.Windows;
using TBCombat.NET.Model;

namespace TBCombat.NET.View
{
    /// <summary>
    /// Interaction logic for InitDialog.xaml
    /// 
    /// Copyright 2016 David Olmos Garrido
    /// </summary>
    public partial class InitDialog : Window
    {
        public InitDialog()
        {
            InitializeComponent();
            comboBox.ItemsSource = Enum.GetValues(typeof(Jugador.Clases));
        }

        public new KeyValuePair<String, Jugador.Clases>? ShowDialog()
        {
            bool? result = base.ShowDialog();
            Jugador.Clases clase;
            if (result == true)
            {
                Enum.TryParse<Jugador.Clases>(comboBox.SelectedValue.ToString(), out clase);
                return new KeyValuePair<string, Jugador.Clases>(textBox.Text, clase);
            }
            return null;
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
