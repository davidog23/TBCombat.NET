﻿using System.Windows;

namespace TBCombat.NET.View
{
    /// <summary>
    /// Interaction logic for FinalDialog.xaml
    /// 
    /// Copyright 2016 David Olmos Garrido 
    /// </summary>
    public partial class FinalDialog : Window
    {
        public FinalDialog(string result)
        {
            InitializeComponent();
            this.result.Content = result;
            this.Title = result;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }
    }
}
