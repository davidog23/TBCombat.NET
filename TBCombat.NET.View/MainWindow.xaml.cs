﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.ServiceModel;
using TBCombat.NET.Model;

namespace TBCombat.NET.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// 
    /// Copyright 2016 David Olmos Garrido
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void onlineBtn_Click(object sender, RoutedEventArgs e)
        {
            InitDialog initDialog = new InitDialog();
            KeyValuePair<string, Jugador.Clases>? inicializador = initDialog.ShowDialog();
            if (inicializador != null)
            {
                OnlineGUI game = new OnlineGUI((KeyValuePair<string, Jugador.Clases>)inicializador, this);
                if (!game.Faulted)
                {
                    Hide();
                    game.Show();
                }
                else
                {
                    Console.WriteLine("Imposible conectar con el servidor.");
                }
            }
        }

        private void offlineBtn_Click(object sender, RoutedEventArgs e)
        {
            InitDialog initDialog = new InitDialog();
            KeyValuePair<string, Jugador.Clases>? inicializador = initDialog.ShowDialog();
            if (inicializador != null)
            {
                OfflineGUI game = new OfflineGUI(Jugador.init((KeyValuePair<string, Jugador.Clases>)inicializador), this);
                Hide();
                game.Show(); 
            }
        }
    }
}
