﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Windows;
using System.Windows.Controls;
using TBCombat.NET.Model;
using TBCombat.NET.Service;
using TBCombat.NET.Utils;

namespace TBCombat.NET.View
{
    /// <summary>
    /// Interaction logic for OnlineGUI.xaml. Client form for the online mode.
    /// 
    /// Copyright 2016 David Olmos Garrido
    /// </summary>
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Reentrant)]
    public partial class OnlineGUI : Window, ServiceManagerProxy.IServiceManagerCallback, IConsoleForm
    {
        Jugador player;
        Jugador adversario;
        StatsPopup tooltipPlayer;
        StatsPopup tooltipIA;
        MainWindow owner;
        ServiceManagerProxy.ServiceManagerClient proxy;
        int attackSelected;
        int AttackSelected
        {
            get
            {
                int aux = attackSelected;
                attackSelected = -1;
                return aux;
            }

            set
            {
                attackSelected = value;
            }
        }

        bool IsDisconnectedFromTheServer = false;
        public bool Faulted { get; set; } = false;

        public TextBox NewConsole { get { return textBox; } }

        public TextWriter OldConsole { get; set; }

        public OnlineGUI(KeyValuePair<string, Jugador.Clases> inicializador, MainWindow owner)
        {
            InstanceContext context = new InstanceContext(this);
            proxy = new ServiceManagerProxy.ServiceManagerClient(context);
            try
            {
                proxy.connect(inicializador);
                player = Jugador.init(inicializador);

                this.owner = owner;
                InitializeComponent();
                Console.SetOut(new ConsoleWriter(this));
                EnableButtons(false);

            }
            catch (EndpointNotFoundException e)
            {
                Console.WriteLine(e.StackTrace);
                owner.Show();
                Faulted = true;
                Close();
            }
            
        }

        public void GetAttack(int ataqueDelAdversario)
        {
            textBox.Clear();
            nextTurn(AttackSelected, ataqueDelAdversario);
            if (player.HP <= 0 || adversario.HP <= 0)
            {
                Close();
            }
            else
            {
                Console.WriteLine(player.SkillMenu(adversario));
                EnableButtons(true);
            }
        }

        private void nextTurn(int ataqueP1, int ataqueP2)
        {
            if (player.Vel > adversario.Vel)
            {
                if (player.WillAttack) { player.Ataques[ataqueP1](adversario); }
                if (adversario.WillAttack) { adversario.Ataques[ataqueP2](player); }
            }
            else if (player.Vel < adversario.Vel)
            {
                if (adversario.WillAttack) { adversario.Ataques[ataqueP2](player); }
                if (player.WillAttack) { player.Ataques[ataqueP1](adversario); }
            }
            else
            {
                Random rand = new Random(DateTime.Now.Millisecond);
                int aux = rand.Next(0, 2);
                if (aux == 0)
                {
                    if (player.WillAttack) { player.Ataques[ataqueP1](adversario); }
                    if (adversario.WillAttack) { adversario.Ataques[ataqueP2](player); }
                }
                else
                {
                    if (adversario.WillAttack) { adversario.Ataques[ataqueP2](player); }
                    if (player.WillAttack) { player.Ataques[ataqueP1](adversario); }
                }
            }
            player.updateState();
            adversario.updateState();
        }

        public void GetOpponentBase(string name, Jugador.Clases clase)
        {
            adversario = Jugador.init(new KeyValuePair<string, Jugador.Clases>(name, clase));
            Console.WriteLine(player.SkillMenu(adversario));
            DataContext = new { player = player, opponent = adversario };

            nameLbl.Content = player.Nombre;
            targetNameLbl.Content = adversario.Nombre;

            tooltipPlayer = new StatsPopup(player);
            nameLbl.ToolTip = tooltipPlayer;

            tooltipIA = new StatsPopup(adversario);
            targetNameLbl.ToolTip = tooltipIA;
            EnableButtons(true);
        }

        public void Write(string s)
        {
            Console.WriteLine();
            Console.WriteLine(s);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (adversario != null && !Faulted)
            {
                string localRes, remoteRes;
                if (player.HP <= 0 && adversario.HP <= 0)
                {
                    localRes = "DRAW";
                    remoteRes = proxy.disconnect(DisconnectStates.Normal);
                }
                else if (adversario.HP <= 0 )
                {
                    localRes = "WIN";
                    remoteRes = proxy.disconnect(DisconnectStates.Normal);
                }
                else if (player.HP <= 0)
                {
                    localRes = "LOST";
                    remoteRes = proxy.disconnect(DisconnectStates.Normal);
                }
                else if(IsDisconnectedFromTheServer)
                {
                    localRes = "WIN";
                    remoteRes = "WIN";
                } 
                else
                {
                    remoteRes = proxy.disconnect(DisconnectStates.Closed);
                    localRes = remoteRes;
                }
                if (localRes.Equals(remoteRes))
                {
                    FinalDialog dialog = new FinalDialog(remoteRes);
                    bool? res = dialog.ShowDialog();
                }
                else
                {
                    Console.Error.WriteLine("El resultado local de la partida no esta sincronizado con el resultado remoto. SYNC ERROR. ARE YOU HACKING THE GAME?");
                    FinalDialog dialog = new FinalDialog(remoteRes);
                }
            }
            else
            {
                proxy.disconnect(DisconnectStates.Queue);
            }
            proxy.Close();
            owner.Show();
        }

        private void button_click(object sender, RoutedEventArgs e)
        {
            EnableButtons(false);
            int ataqueSeleccionado = ((Button)sender).Content.ToString().Last() - '1';
            AttackSelected = ataqueSeleccionado;
            proxy.sendAttack(ataqueSeleccionado);
        }

        public void DisconnectedByTheServer()
        {
            IsDisconnectedFromTheServer = true;
            /*FinalDialog f = new FinalDialog("WIN");
            f.ShowDialog();*/
            Close();
        }

        private void EnableButtons(bool b)
        {
            foreach (Button btn in ButtonWrapper.Children.OfType<Button>())
            {
                btn.IsEnabled = b;
            }
        }
    }
}
