﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using TBCombat.NET.Model;
using TBCombat.NET.Utils;

namespace TBCombat.NET.View
{
    /// <summary>
    /// Interaction logic for OfflineGUI.xaml
    /// 
    /// Copyright 2016 David Olmos Garrido
    /// </summary>
    public partial class OfflineGUI : Window, IConsoleForm
    {
        Jugador player;
        Jugador ia;
        StatsPopup tooltipPlayer;
        StatsPopup tooltipIA;
        Window owner;

        public TextBox NewConsole { get { return textBox; } }

        public TextWriter OldConsole { get; set; }

        public OfflineGUI(Jugador player, Window owner)
        {
            this.player = player;
            ia = Jugador.init();
            this.owner = owner;
            Console.SetOut(new ConsoleWriter(this));
            InitializeComponent();
            DataContext = new { player = player, ia = ia };
            nameLbl.Content = player.Nombre;
            targetNameLbl.Content = ia.Nombre;

            tooltipPlayer = new StatsPopup(player);
            nameLbl.ToolTip = tooltipPlayer;

            tooltipIA = new StatsPopup(ia);
            targetNameLbl.ToolTip = tooltipIA;

            Console.WriteLine(player.SkillMenu(ia));
        }

        public void nextTurn(int ataqueP1, int ataqueP2)
        {
            if (player.Vel > ia.Vel)
            {
                if (player.WillAttack) { player.Ataques[ataqueP1](ia); }
                if (ia.WillAttack) { ia.Ataques[ataqueP2](player); }
            }
            else if (player.Vel < ia.Vel)
            {
                if (ia.WillAttack) { ia.Ataques[ataqueP2](player); }
                if (player.WillAttack) { player.Ataques[ataqueP1](ia); }
            }
            else {
                Random rand = new Random(DateTime.Now.Millisecond);
                int aux = rand.Next(0, 2);
                if (aux == 0)
                {
                    if (player.WillAttack) { player.Ataques[ataqueP1](ia); }
                    if (ia.WillAttack) { ia.Ataques[ataqueP2](player); }
                }
                else
                {
                    if (ia.WillAttack) { ia.Ataques[ataqueP2](player); }
                    if (player.WillAttack) { player.Ataques[ataqueP1](ia); }
                }
            }
            player.updateState();
            ia.updateState();
        }

        private void button_click(object sender, RoutedEventArgs e)
        {
            textBox.Clear();
            int ataqueSeleccionado = ((Button)sender).Content.ToString().Last() - '1';
            nextTurn(ataqueSeleccionado, ia.seleccionAtaqueIA(player));

            if (player.HP <= 0 || ia.HP <= 0)
            {
                string result;
                if (player.HP <= 0 && ia.HP <= 0)
                {
                    result = "DRAW";
                }
                else if (ia.HP <= 0)
                {
                    result = "WIN";
                }
                else
                {
                    result = "LOST";
                }
                FinalDialog dialog = new FinalDialog(result);
                bool? res = dialog.ShowDialog();
                Close();
            }
            else
            {
                Console.WriteLine(player.SkillMenu(ia));
            }
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            Console.SetOut(OldConsole);
            owner.Show();
        }
    }
}
