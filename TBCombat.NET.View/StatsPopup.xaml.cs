﻿using System.Collections.Generic;
using System.Windows.Controls;
using TBCombat.NET.Model;

namespace TBCombat.NET.View
{
    /// <summary>
    /// Interaction logic for StatsPopup.xaml
    /// 
    /// Copyright 2016 David Olmos Garrido
    /// </summary>
    public partial class StatsPopup : UserControl
    {
        Jugador player;

        public StatsPopup(Jugador player)
        {
            this.player = player;
            player.StatesUpdated += new Jugador.UpdateStatesEventHandler(states_updated);
            InitializeComponent();
            listBox.DataContext = player;
        }

        public void states_updated(Jugador sender, List<Estado.Estados> states)
        {
            int separatorIndex = listBox.Items.IndexOf(separator);
            int finalIndex = listBox.Items.Count - 1;
            while(finalIndex > separatorIndex)
            {
                listBox.Items.RemoveAt(finalIndex);
                finalIndex--;
            }

            foreach(var state in states)
            {
                listBox.Items.Add(state);
            }
        }
    }
}
