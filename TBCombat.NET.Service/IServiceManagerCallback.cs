﻿using System.ServiceModel;
using TBCombat.NET.Model;

namespace TBCombat.NET.Service
{
    /// <summary>
    /// Contract for the ServiceManager contract. Implemeted at OnlineGUI.xaml.cs
    /// 
    /// Copyright 2016 David Olmos Garrido
    /// </summary>
    public interface IServiceManagerCallback
    {
        [OperationContract(IsOneWay = true)]
        void GetAttack(int ataqueDelAdversario);
        [OperationContract(IsOneWay = true)]
        void GetOpponentBase(string name, Jugador.Clases clase);
        [OperationContract(IsOneWay = true)]
        void Write(string s);
        [OperationContract(IsOneWay = true)]
        void DisconnectedByTheServer();
    }
}
