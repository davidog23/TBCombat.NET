﻿using System.Collections.Generic;
using System.ServiceModel;
using TBCombat.NET.Model;

namespace TBCombat.NET.Service
{
    /// <summary>
    /// Contract for the ServiceManager. It is supposed to be the ServiceHost for the game.
    /// 
    /// Copyright 2016 David Olmos Garrido
    /// </summary>
    [ServiceContract(CallbackContract = typeof(IServiceManagerCallback))]
    public interface IServiceManager
    {
        /// <summary>
        /// Connects to server.
        /// </summary>
        /// <param name="inicializador">The initializer of the player's client</param>
        [OperationContract(IsOneWay = true)]
        void connect(KeyValuePair<string, Jugador.Clases> inicializador);
        /// <summary>
        /// Sends the selected attack to the server.
        /// </summary>
        /// <param name="ataque">The index at the Attack Array at Jugador</param>
        [OperationContract(IsOneWay = true)]
        void sendAttack(int ataque);
        /// <summary>
        /// Discconect the client from the server.
        /// </summary>
        /// <returns>The message for the FinalDialog(WIN, LOST, DRAW)</returns>
        [OperationContract(IsOneWay = false)]
        string disconnect(DisconnectStates state);
    }
}
