﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TBCombat.NET.Model;
using System.ServiceModel;

namespace TBCombat.NET.Service
{
    /// <summary>
    /// Implementation of IServiceManager.
    /// 
    /// Copyright 2016 David Olmos Garrido
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class ServiceManager : IServiceManager
    {

        private readonly Dictionary<string, Client> queue = new Dictionary<string, Client>();
        private readonly Dictionary<string, KeyValuePair<Client, Client>> clients = new Dictionary<string, KeyValuePair<Client, Client>>();

        public void connect(KeyValuePair<string, Jugador.Clases> inicializador)
        {
            string myId = OperationContext.Current.SessionId;
            Debug.Assert(myId != null, "myId != null");
            IServiceManagerCallback callback = OperationContext.Current.GetCallbackChannel<IServiceManagerCallback>();
            if (queue.Count == 0)
            {
                queue.Add(myId, value: new Client { Context = OperationContext.Current, Callback = callback, Player = Jugador.init(inicializador), SessionId = myId });
            }
            else
            {
                string otherId = queue.Keys.First();
                KeyValuePair<Client, Client> pareja = new KeyValuePair<Client, Client>(new Client { Context = OperationContext.Current, Callback = callback, Player = Jugador.init(inicializador), SessionId = myId }, queue[otherId]);
                clients.Add(myId, pareja);
                clients.Add(otherId, pareja);
                queue.Remove(otherId);
                pareja.Key.Callback.GetOpponentBase(pareja.Value.Player.Nombre, pareja.Value.Player.Clase);
                pareja.Value.Callback.GetOpponentBase(pareja.Key.Player.Nombre, pareja.Key.Player.Clase);
            }
        }

        public void sendAttack(int ataque)
        {
            string myId = OperationContext.Current.SessionId;
            Debug.Assert(myId != null, "myId != null");
            Client current, waiting;
            if (clients[myId].Key.SessionId == myId)
            {
                current = clients[myId].Key;
                waiting = clients[myId].Value;
            }
            else
            {
                current = clients[myId].Value;
                waiting = clients[myId].Key;
            }

            int ataqueAdversario = waiting.AttackSelected;
            if (ataqueAdversario != -1)
            {
                current.Callback.GetAttack(ataqueAdversario);
                waiting.Callback.GetAttack(ataque);
                NextTurn(ataque, ataqueAdversario, current.Player, waiting.Player);
            }
            else
            {
                current.AttackSelected = ataque;
            }
        }

        private static void NextTurn(int ataqueP1, int ataqueP2, Jugador player1, Jugador player2)
        {
            if (player1.Vel > player2.Vel)
            {
                if (player1.WillAttack) { player1.Ataques[ataqueP1](player2); }
                if (player2.WillAttack) { player2.Ataques[ataqueP2](player1); }
            }
            else if (player1.Vel < player2.Vel)
            {
                if (player2.WillAttack) { player2.Ataques[ataqueP2](player1); }
                if (player1.WillAttack) { player1.Ataques[ataqueP1](player2); }
            }
            else
            {
                Random rand = new Random(DateTime.Now.Millisecond);
                int aux = rand.Next(0, 2);
                if (aux == 0)
                {
                    if (player1.WillAttack) { player1.Ataques[ataqueP1](player2); }
                    if (player2.WillAttack) { player2.Ataques[ataqueP2](player1); }
                }
                else
                {
                    if (player2.WillAttack) { player2.Ataques[ataqueP2](player1); }
                    if (player1.WillAttack) { player1.Ataques[ataqueP1](player2); }
                }
            }
            player1.updateState();
            player2.updateState();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public string disconnect(DisconnectStates state)
        {
            if (state != DisconnectStates.Queue)
            {
                string myId = OperationContext.Current.SessionId;
                Debug.Assert(myId != null, "myId != null");
                Client current, other;
                if (clients[myId].Key.SessionId == myId)
                {
                    current = clients[myId].Key;
                    other = clients[myId].Value;
                }
                else
                {
                    current = clients[myId].Value;
                    other = clients[myId].Key;
                }

                clients.Remove(myId);
                if (state == DisconnectStates.Closed)
                {
                    other.Callback.DisconnectedByTheServer();
                    return "LOST";
                }
                else
                {
                    if (current.Player.HP <= 0 && other.Player.HP <= 0)
                    {
                        return "DRAW";
                    }
                    else if (other.Player.HP <= 0)
                    {
                        return "WIN";
                    }
                    else
                    {
                        return "LOST";
                    }
                } 
            }
            else
            {
                queue.Remove(OperationContext.Current.SessionId);
                return "";
            }
        }
    }

    internal class Client
    {
        public OperationContext Context { get; set; }
        public IServiceManagerCallback Callback { get; set; }
        public Jugador Player { get; set; }
        public string SessionId { get; set; }
        int attackSelected = -1;
        public int AttackSelected
        {
            get
            {
                int aux = attackSelected;
                attackSelected = -1;
                return aux;
            }

            set
            {
                attackSelected = value;
            }
        }
    }

    public enum DisconnectStates
    {
        Closed, Normal, Queue
    } 
}
